//
//  ColorTransferDelegate.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/15/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import UIKit

protocol ColorTransferDelegate {
    func userDidChoose(color: UIColor, withName colorName: String)
}
