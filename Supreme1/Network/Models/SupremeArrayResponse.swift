//
//  SupremeArrayResponse.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/17/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper

class SupremeArrayResponse<T: Mappable>: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status  <- map["status"]
        message <- map["message"]
        data    <- map["data"]
        error   <- map["error"]
    }
    
    var status: Bool?
    var message: String?
    var data: [T]?
    var error: String?
}

