//
//  UserRequestDelegate.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/17/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper

protocol UserDelegate {
    func onUserResponse(response: SupremeResponse<User>?)
    func onUserPlaylistResponse(response: SupremeArrayResponse<UserPlaylist>?)
    func onUserAccountResponse(response: SupremeArrayResponse<SupremeAccount>?)
    
}
