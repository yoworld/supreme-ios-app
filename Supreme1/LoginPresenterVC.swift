//
//  LoginPresenterVC.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/12/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import UIKit

class LoginPresenterVC: UIViewController, ColorTransferDelegate, UserDelegate{
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var colorNameLbl: UILabel!
    var mainTabBarView: MainTabBarViewController?
    
    func onUserResponse(response: SupremeResponse<User>?) {
        print(response?.data?.name ?? "IS NULL")
        if(mainTabBarView == nil)
        {
            mainTabBarView = self.storyboard?.instantiateViewController(withIdentifier: "mainTab") as! MainTabBarViewController
        }
        present(mainTabBarView!, animated: true, completion: nil)
    }
    
    func onUserPlaylistResponse(response: SupremeArrayResponse<UserPlaylist>?){
        let data = response?.data
        if let data = data {
            for i in 0..<data.count {
                print(data[i].title!)
            }
        }
    }
    
    func onUserAccountResponse(response: SupremeArrayResponse<SupremeAccount>?) {
        let data = response?.data
        if let data = data {
            for i in 0..<data.count {
                print(data[i].user_name!)
            }
        }
    }
    
    @IBAction func onLoginBtnPressed(_ sender: UIButton) {
        var email: String = emailTextField.text!
        var password: String = passwordTextField.text!
        SupremeService.getCurrentUser(delegate: self)
        SupremeService.getUserPlaylist(delegate: self)
        SupremeService.getUserAccounts(user_id: 1, delegate: self)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ColorPickerVC.IDENTIFIER {
            guard let colorPickerVC = segue.destination as? ColorPickerVC else { return }
            colorPickerVC.delegate = self
        }
    }
    
    func userDidChoose(color: UIColor, withName colorName: String) {
        colorNameLbl.text = colorName
        view.backgroundColor = color
    }
    
    
}

