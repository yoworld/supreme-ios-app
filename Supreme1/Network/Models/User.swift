//
//  User.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/16/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper
class User : Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        email <- map["email"]
        user_ip <- map["user_ip"]
        user_ipv6 <- map["user_ipv6"]
        user_banned <- map["user_banned"]
        mod_hash <- map["mod_hash"]
        active <- map["active"]
        api_token <- map["api_token"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        level <- map["level"]
        user_last_auth <- map["user_last_auth"]
        mod_title <- map["mod_title"]
        
        
    }
    
    var id: Int?
    var name: String?
    var email: String?
    var user_ip: String?
    var user_ipv6: String?
    var user_banned: Int?
    var mod_hash: String?
    var active: Bool?
    var api_token: String?
    var created_at: String?
    var updated_at: String?
    var level: Int?
    var user_last_auth: String?
    var mod_title: String?
}
