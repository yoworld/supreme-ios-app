//
//  SupremeResponse.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/16/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper

class SupremeResponse<T: Mappable>: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status  <- map["status"]
        message <- map["message"]
        data    <- map["data"]
        error   <- map["error"]
    }
    
    var status: Bool?
    var message: String?
    var data: T?
    var error: String?
}
