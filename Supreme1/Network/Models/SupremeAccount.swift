//
//  SupremeAccount.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/17/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper

class SupremeAccount: Mappable {
    
    var user_id: Int?
    var user_name: String?
    var user_banned: Bool?
    var user_ip: String?
    var user_ipv6: String?
    var created_at: String?
    var updated_at: String?
    var user_facebook_id: String?
    var user_snapi_auth: String?
    var user_network_credentials: String?
    var user_yo_auth_key: String?
    var user_lk: String?
    var user_settings: Settings?
    var user_level: Int?
    var deleted_at: String?
    var user_active: Bool?
    var user_last_auth: String?
    var assigned_to: Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        user_name <- map["user_name"]
        user_banned <- map["user_banned"]
        user_ip <- map["user_ip"]
        user_ipv6 <- map["user_ipv6"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        user_facebook_id <- map["user_facebook_id"]
        user_snapi_auth <- map["user_snapi_auth"]
        user_network_credentials <- map["user_network_credentials"]
        user_yo_auth_key <- map["user_yo_auth_key"]
        user_lk <- map["user_lk"]
        user_settings <- map["user_settings"]
        user_level <- map["user_level"]
        deleted_at <- map["deleted_at"]
        user_active <- map["user_active"]
        user_last_auth <- map["user_last_auth"]
        assigned_to <- map["assigned_to"]
    }
    
    
}
