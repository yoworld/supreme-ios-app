//
//  SupremeService.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/16/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class SupremeService {
    
    static let debug : Bool = false
    
    static let DEBUG_BASE_URL = "http://supreme.ru"
    static let PROD_BASE_URL = "https://yoprice.co"
    
    enum ENDPOINT : String {
        case PLAYLIST = "/api/v1/playlist"
        case CURRENT = "/api/v1/current"
        case USER = "/api/v1/user"
        case SUPREMES = "/api/v1/supreme"
        case CHARACTERS = "/api/v1/characters"
        case VERSION = "/api/v1/version"
    }
    
    
    //region
    
    //endregion
    
    private static func getBaseUrl() -> String {
        return debug ? DEBUG_BASE_URL : PROD_BASE_URL
    }
    
    
    public static func getUserAccounts(user_id: Int, delegate: UserDelegate){
        Alamofire.request("\(getBaseUrl())\(ENDPOINT.USER.rawValue)/\(user_id)/account")
            .validate()
            .responseJSON {
                response in
                switch(response.result){
                case .success(_):
                    if let data = response.result.value {
                        let result = Mapper<SupremeArrayResponse<SupremeAccount>>().map(JSONObject: data)
                        delegate.onUserAccountResponse(response: result)
                    }
                    break
                case .failure(_):
                    break;
                }
        }
    }
    
    public static func getUserPlaylist(delegate: UserDelegate){
        Alamofire.request("\(getBaseUrl())\(ENDPOINT.PLAYLIST.rawValue)")
            .validate()
            .responseJSON {
                response in
                switch(response.result){
                case .success(_):
                    if let data = response.result.value {
                        let result = Mapper<SupremeArrayResponse<UserPlaylist>>().map(JSONObject: data)
                        delegate.onUserPlaylistResponse(response: result)
                    }
                    break
                case .failure(_):
                    break;
                }
        }
    }
    
    public static func getCurrentUser(delegate: UserDelegate) {
        Alamofire.request("\(getBaseUrl())\(ENDPOINT.CURRENT.rawValue)")
            .validate()
            .responseJSON {
                response in
                switch(response.result){
                case .success(_):
                    if let data = response.result.value {
                        let result = Mapper<SupremeResponse<User>>().map(JSONObject: data)
                        delegate.onUserResponse(response: result)
                    }
                    break
                case .failure(_):
                    break;
                }
        }
    }
}
