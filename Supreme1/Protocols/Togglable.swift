//
//  Togglable.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/16/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation

protocol Togglable {
    mutating func toggle()
}
