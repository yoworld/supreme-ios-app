//
//  Settings.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/17/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper

class Settings: Mappable {
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        rainbow <- map["rainbow"]
        spoofer <- map["spoofer"]
        frequency <- map["frequency"]
        lfrequency <- map["lfrequency"]
        stalker <- map["stalker"]
        copycat <- map["copycat"]
        chaosmode <- map["chaosmode"]
        canfreeze <- map["canfreeze"]
        summoned <- map["summoned"]
        chatrepeater <- map["chatrepeater"]
        canttt <- map["canttt"]
        ignore <- map["ignore"]
    }
    
    var rainbow: Bool?
    var spoofer: Bool?
    var frequency: Int?
    var lfrequency: Int?
    var stalker: Bool?
    var copycat: Bool?
    var chaosmode: Bool?
    var canfreeze: Bool?
    var summoned: String?
    var chatrepeater: Bool?
    var canttt: Bool?
    var ignore: [Int]?
}
