//
//  UserPlaylist.swift
//  Supreme1
//
//  Created by Charlton Smith on 2/17/18.
//  Copyright © 2018 Charlton Smith. All rights reserved.
//

import Foundation
import ObjectMapper

class UserPlaylist: Mappable {
    
    var user_id: Int?
    var youtube_id: String?
    var position: Int?
    var created_at: String?
    var updated_at: String?
    var title: String?
    var jpg_link: String?
    var mp3_link: String?
    var composite_key: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        user_id <- map["user_id"]
        youtube_id <- map["youtube_id"]
        position <- map["position"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        title <- map["title"]
        jpg_link <- map["jpg_link"]
        mp3_link <- map["mp3_link"]
        composite_key <- map["composite_key"]
    }
}
